document.querySelector('#test').addEventListener('click',() => {
    const data = [
        {
            'id': 'de',
            'name': 'de',
            'category': 'de',
        },{
            'id': 'de',
            'name': 'de',
            'category': 'fr',
        },{
            'id': 'de',
            'name': 'de',
            'category': 'de',
        },{
            'id': 'de',
            'name': 'de',
            'category': 'az',
        },{
            'id': 'de',
            'name': 'de',
            'category': 'de',
        },{
            'id': 'de',
            'name': 'de',
            'category': 'fr',
        },
    ]
    // Liste toutes les category
    const categoryResult = [];
    data.forEach((key) => {
        if(!categoryResult.includes(key.category)) {
            categoryResult.push(key.category)
        }
    });

    //Return la liste des tricks d'une category
    function myTricks(category) {
        const myTricksList = [];
        data.forEach((key) => {
            if(key.category == category) {
                myTricksList.push(key)
            }
        });
        return myTricksList;
    }

    console.log('Début Test');
    console.log('Data : '+JSON.stringify(data));
    console.log('categories : '+categoryResult);
    console.log('test tricks : '+myTricks('de'));
    console.log('Fin Test');
    /*const fullList = categoryResult.map((key) => {
        return (
            <div>
                <LessonH2 name={key.name} />

                <ul className={styles.lessonsList}>
                    {myTricks(key.name)}
                </ul>
            </div>
        )
    })

    return (
        <section className={styles.lessonsContainer}>
            {fullList}
        </section>
    );*/
});