export default function Lessons() {
    const categoryResult = [];
    const categoryMap = new Map();
    for (const item of data) {
        if(!categoryMap.has(item.category)) {
            categoryMap.set(item.category, true);
            categoryResult.push({
                name: item.category
            });
        }
    }

    function myTricks(category) {
        const myTricksList = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].category === category) {
                myTricksList.push(<li className={styles.lessonsElement}><Lesson name={data[i].name} /></li>);
            }
        }
        return myTricksList;
    }

    const fullList = categoryResult.map((key) => {
        return (
            <div>
                <LessonH2 name={key.name} />

                <ul className={styles.lessonsList}>
                    {myTricks(key.name)}
                </ul>
            </div>
        )
    })

    return (
        <section className={styles.lessonsContainer}>
            {fullList}
        </section>
    );
}