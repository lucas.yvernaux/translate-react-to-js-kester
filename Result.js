export default function Lessons() {
    //Initial categoryResult
    const categoryResult = [];
    const categoryMap = new Map();
    for (const item of data) {
        if(!categoryMap.has(item.category)) {
            categoryMap.set(item.category, true);
            categoryResult.push({
                name: item.category
            });
        }
    }
    // Lucas category
    // const categoryResult = [];
    // data.forEach((key) => {
    //     if(!categoryResult.includes(key.category)) {
    //         categoryResult.push(key.category)
    //     }
    // });

    //Initial myTricks
    function myTricks(category) {
        const myTricksList = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].category === category) {
                myTricksList.push(<li className={styles.lessonsElement}><Lesson name={data[i].name} /></li>);
            }
        }
        return myTricksList;
    }

    //Lucas myTricks React
    // function myTricks(category) {
    //     const myTricksList = [];
    //     data.forEach((key) => {
    //         if(key.category == category) {
    //             myTricksList.push(<li className={styles.lessonsElement}><Lesson name={key.name} /></li>)
    //         }
    //     });
    //     return myTricksList;
    // }
    //Lucas myTricks JS
    // function myTricks(category) {
    //     const myTricksList = [];
    //     data.forEach((key) => {
    //         if(key.category == category) {
    //             myTricksList.push(key)
    //         }
    //     });
    //     return myTricksList;
    // }

    //Initial
    const fullList = categoryResult.map((key) => {
        return (
            <div>
                <LessonH2 name={key.name} />

                <ul className={styles.lessonsList}>
                    {myTricks(key.name)}
                </ul>
            </div>
        )
    })
    //Lucas fullList React
    // const fullList = categoryResult.map((key) => {
    //     return (
    //         <div>
    //             <LessonH2 name={key} />
    //
    //             <ul className={styles.lessonsList}>
    //                 {myTricks(key)}
    //             </ul>
    //         </div>
    //     )
    // })

    return (
        <section className={styles.lessonsContainer}>
            {fullList}
        </section>
    );
}